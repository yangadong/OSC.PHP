OSC.PHP
-------------
开源中国（OsChina.NET）的账号登录及api操作，使用oauth 2.0  
提供简单的账号登录、获取个人信息、发布动弹等功能，如果需要其他功能可以根据官方的api文档自行添加

    //示例：获取当前登录用户的账户信息
    $result=$osc->api('user', array(), 'GET');

文件说明
-------------
>**osc.php** 主文件  
>**demo.php** 示例程序  
>**config.php** 示例程序配置  
>**callback.php** 示例程序回调文件

开发者信息
-------------
[PiscDong studio](http://www.piscdong.com/)
