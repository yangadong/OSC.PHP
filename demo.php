<?php
session_start(); //此示例中要使用session
require_once('config.php');
require_once('osc.php');

$osc_t=isset($_SESSION['osc_t'])?$_SESSION['osc_t']:'';

//检查是否已登录
if($osc_t!=''){
	$osc=new oscPHP($osc_id, $osc_s, $osc_t);

	//获取当前登录用户的账户信息
	$result=$osc->me();
	var_dump($result);

	/**
	//发布动弹
	$content='动弹内容';
	$result=$osc->tweet_pub($content);
	var_dump($result);
	**/

	/**
	//获取最新动弹列表
	$result=$osc->tweet_list();
	var_dump($result);
	**/

	/**
	//其他功能请根据官方文档自行添加
	//示例：获取当前登录用户的账户信息
	$result=$osc->api('user', array(), 'GET');
	var_dump($result);
	**/

}else{
	//生成登录链接
	$osc=new oscPHP($osc_id, $osc_s);
	$login_url=$osc->login_url($callback_url);
	echo '<a href="',$login_url,'">点击进入授权页面</a>';
}
